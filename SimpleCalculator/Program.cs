﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack rpn = new Stack();

            while (true)
            {
                string str = Console.ReadLine();
                rpn.process(str);

                Console.WriteLine("=" + rpn.result());
            }
        }
    }

    class Stack
    {
        private List<int> theStack;

        public void process(string value)
        {
            int intVal = 0;
            int result = 0;
            int leftInteger, rightInteger;

            switch (value)
            {
                case "+":
                    leftInteger = pop();
                    rightInteger = pop();
                    result = addNumbers(leftInteger, rightInteger);
                    break;
                case "-":
                    leftInteger = pop();
                    rightInteger = pop();
                    result = subtractNumbers(leftInteger,rightInteger);
                    break;
                case "*":
                    leftInteger = pop();
                    rightInteger = pop();
                    result = multiplyNumbers(leftInteger, rightInteger);
                    break;
                case "/":
                    leftInteger = pop();
                    rightInteger = pop();
                    result = divideNumbers(leftInteger, rightInteger);
                    break;
                default:
                    if (int.TryParse(value, out intVal))
                    {
                        this.theStack.Add(intVal);
                    }
                    else
                    {
                        Console.WriteLine("No action taken.  Input was neither an operation nor an integer.");
                    }
                    break;
                }
        }

        public int result()
        {
            if (theStack.Count > 0)
            {
                return theStack[theStack.Count - 1];
            }

            return -1;
        }

        private void push(int value)
        {
            if (value != null)
            {
                this.theStack.Add(value);
            }
            else
            {
                Console.WriteLine("Failed to push.  The value provided was empty!");
            }
        }

        private int pop()
        {
            if ((this.theStack.Count - 1) > 0)
            {
                int result = this.theStack[this.theStack.Count - 1];
                this.theStack.RemoveAt(this.theStack.Count - 1);

                return result;
            }
            else
            {
                Console.WriteLine("Failed to pop.  The stack is empty!");
                return -1;
            }
        }

        private int addNumbers(int leftInteger, int rightInteger)
        {
            if (this.theStack.Count > 2)
            {
                return leftInteger + rightInteger;
            }
            else
            {
                Console.WriteLine("Failed to obtain two numbers to add together!");
                return -1;
            }
        }

        private int subtractNumbers(int leftInteger, int rightInteger)
        {
            if (this.theStack.Count > 2)
            {
                return leftInteger - rightInteger;
            }
            else
            {
                Console.WriteLine("Failed to obtain two numbers to subtract together!");
                return -1;
            }
        }

        private int multiplyNumbers(int leftInteger, int rightInteger)
        {
            if (this.theStack.Count > 2)
            {
                return leftInteger * rightInteger;
            }
            else
            {
                Console.WriteLine("Failed to obtain two numbers to multiply together!");
                return -1;
            }
        }

        private int divideNumbers(int leftInteger, int rightInteger)
        {
            if (this.theStack.Count > 2)
            {
                return leftInteger / rightInteger;
            }
            else
            {
                Console.WriteLine("Failed to obtain two numbers to divide together!");
                return -1;
            }
        }
    }
}
